use warnings;
use strict;
use Data::Dumper;

sub translate_peptide{
	my ($pep, $mod) = @_;
	chomp $mod;
	my %detected_mods;

	if ($mod =~ /\((\D+)\* (\+|\-)(\d+\.\d+)/){
		my $delta = $2 . $3;
		if((15.5 < $delta) and (16.5 > $delta)){
			$detected_mods{'\*'} = '(35)';
		}
		if((-18.5 < $delta) and (-17.5 > $delta)){
			$detected_mods{'\*'} = '(23)';
		}
		if((79.5 < $delta) and (81.5 > $delta)){
			$detected_mods{'\*'} = '(21)';
		}
		if((143.6 < $delta) and (144.6 > $delta)){
			$detected_mods{'\*'} = '(214)';
		}
		if((228.7 < $delta) and (229.7 > $delta)){
			$detected_mods{'\*'} = '(737)';
		}
	}
	if ($mod =~ /\((\D+)\# (\+|\-)(\d+\.\d+)/){
		my $delta = $2.$3;
		if((15.5 < $delta) and (16.5 > $delta)){
			$detected_mods{'\#'} = '(35)';
		}
		if((-18.5 < $delta) and (-17.5 > $delta)){
			$detected_mods{'\#'} = '(23)';
		}
		if((79.5 < $delta) and (81.5 > $delta)){
			$detected_mods{'\#'} = '(21)';
		}
		if((143.6 < $delta) and (144.6 > $delta)){
			$detected_mods{'\#'} = '(214)';
		}
		if((228.7 < $delta) and (229.7 > $delta)){
			$detected_mods{'\#'} = '(737)';
		}
	}
	if($mod =~ /\((\D+)\@ (\+|\-)(\d+\.\d+)/){
		my $delta = $2.$3;
		if ((15.5 < $delta) and (16.5 > $delta)){
			$detected_mods{'\@'}='(35)';
		}
		if ((-18.5 < $delta) and (-17.5 > $delta)){
			$detected_mods{'\@'}='(23)';
		}
		if ((79.5 < $delta) and (81.5 > $delta)){
			$detected_mods{'\@'}='(21)';
		}
		if ((143.6 < $delta)and(144.6 > $delta)){
			$detected_mods{'\@'}='(214)';
		}
		if ((228.7 < $delta) and (229.7 > $delta)){
			$detected_mods{'\@'} = '(737)';
		}
	}
	if ($mod =~ /\((\D+)\^ (\+|\-)(\d+\.\d+)/){
		my $delta = $2.$3;
		if((15.5<$delta)and(16.5>$delta)){
			$detected_mods{'\^'}='(35)';
		}
		if((-18.5 < $delta)and(-17.5 > $delta)){
			$detected_mods{'\^'}='(23)';
		}
		if((79.5<$delta)and(81.5>$delta)){
			$detected_mods{'\^'}='(21)';
		}
		if((143.6<$delta)and(144.6>$delta)){
			$detected_mods{'\^'}='(214)';
		}
		if((228.7<$delta)and(229.7>$delta)){
			$detected_mods{'\^'}='(737)';
		}
	}
	if($mod=~/\((\D+)\~ (\+|\-)(\d+\.\d+)/){
		my $delta = $2.$3;
		if((15.5<$delta)and(16.5>$delta)){
			$detected_mods{'\~'} = '(35)';
		}
		if((-18.5<$delta)and(-17.5>$delta)){
			$detected_mods{'\~'} = '(23)';
		}
		if((79.5<$delta)and(81.5>$delta)){
			$detected_mods{'\~'} = '(21)';
		}
		if((143.6<$delta)and(144.6>$delta)){
			$detected_mods{'\~'} = '(214)';
		}
		if((228.7<$delta)and(229.7>$delta)){
			$detected_mods{'\~'} = '(737)';
		}
	}
	if($mod=~/\((\D+)\$ (\+|\-)(\d+\.\d+)/){
		#print $1," -> ",$2," -> ",$3,"\n";
			my $delta = $2.$3;
		if((15.5<$delta)and(16.5>$delta)){
			$detected_mods{'\$'}='(35)';
		}
		if((-18.5<$delta)and(-17.5>$delta)){
			$detected_mods{'\$'}='(23)';
		}
		if((79.5<$delta)and(81.5>$delta)){
			$detected_mods{'\$'}='(21)';
		}
		if((143.6<$delta)and(144.6>$delta)){
			$detected_mods{'\$'}='(214)';
		}
		if((228.7<$delta)and(229.7>$delta)){
			$detected_mods{'\$'}='(737)';
		}
	}

	if($mod=~/\((\D+)\] (\+|\-)(\d+\.\d+)/){
			my $delta = $2.$3;
		if((143.6<$delta)and(144.6>$delta)){
			$detected_mods{'\]'} = '(214)';
		}
		if((228.7<$delta)and(229.7>$delta)){
			$detected_mods{'\]'} = '(737)';
		}
	}
	foreach(keys %detected_mods){
		$pep =~ s/$_/$detected_mods{$_}/g;
		#print $detected_mods{$_}," -> ",$_,"\n";
	}
	return $pep;

}
