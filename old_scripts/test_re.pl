use warnings;
use strict;
use Data::Dumper;

my $mod = '(AMER~ +2.3)';
my @signs = ('\*', '\^', '\#', '\@', '\~', '\$', '\]');

foreach my $sign(@signs){
    my $pat = '\((\D+)' . $sign . ' (\+|\-)(\d+\.\d+)';

    if ($mod =~ /$pat/){
        print 'sign ->', $sign, "\n";
        print $2 . $3, "\n";
    }
}
