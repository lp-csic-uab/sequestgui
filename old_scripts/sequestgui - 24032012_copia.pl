#!/usr/bin/perl -w --

use utf8;
use Wx 0.15 qw[:allclasses];
use strict;
use warnings;
use Data::Dumper;

package MySequestguiFrame;

use Wx qw[:everything];
use Wx::Event qw(EVT_BUTTON);
use warnings;
use strict;

use SequestguiFrame;
use base qw(SequestguiFrame);

my $title = "SequestGui 0.3a";
my $sequest_path = "C:\\Xcalibur\\system\\programs\\BioworksBrowser\\sequest.exe";
my $xc_threshold = 0.2;

sub new {
	my $class = shift;
	my $self = $class->SUPER::new();

	$self->SetTitle($title);
    $self->{tc_sp}->SetValue($sequest_path);
    $self->{tc_xt}->SetValue($xc_threshold);
    $self->{chbx_2}->SetValue(1);
    $self->{chbx_3}->SetValue(1);

    EVT_BUTTON( $self, $self->{bt_mgf_1}, \&set_mgf_1);
	EVT_BUTTON( $self, $self->{bt_mgf_2}, \&set_mgf_2);
	EVT_BUTTON( $self, $self->{bt_mgf_3}, \&set_mgf_3);

    EVT_BUTTON( $self, $self->{bt_par_1}, \&set_par_1);
	EVT_BUTTON( $self, $self->{bt_par_2}, \&set_par_2);
    EVT_BUTTON( $self, $self->{bt_par_3}, \&set_par_3);

	EVT_BUTTON( $self, $self->{bt_sp}, \&set_exec);
	EVT_BUTTON( $self, $self->{bt_of}, \&set_outfile);
    EVT_BUTTON( $self, $self->{bt_run}, \&runit);

	bless($self, $class); # reconsecrate
    return $self;
}


sub set_mgf_1 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_1');
}

sub set_mgf_2 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_2');
}

sub set_mgf_3 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_3');
}

sub set_par_1{
	my ($self, $event) = @_;
    $self->get_files('tc_par_1');
}

sub set_par_2{
	my ($self, $event) = @_;
    $self->get_files('tc_par_2');
}

sub set_par_3{
	my ($self, $event) = @_;
    $self->get_files('tc_par_3');
}

sub get_dir {
	my ($self, $win) = @_;
	my $dialog = Wx::DirDialog->new(undef, 'Select a directory with the mgf files', '');
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $dir = $dialog->GetPath;
	$self->{$win}->SetValue($dir);
}

sub get_files{
    my ($self, $win) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select a .params file', '',
                                     ".params", "*.params", Wx::wxFD_OPEN);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{$win}->SetValue($default_dir . '\\' . $filename);
}


sub set_exec{
	my ($self, $event) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select the Sequest executable', '',
                                     ".exe", "*.exe", Wx::wxFD_OPEN);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{tc_sp}->SetValue($default_dir . '\\' . $filename);
}

sub set_outfile{
	my ($self, $event) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select the Results file', '',
                                     ".xls", "*.xls", Wx::wxFD_SAVE);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{tc_of}->SetValue($default_dir . '\\' . $filename);
}

sub runit {
	my ($self, $event) = @_;
	print "RUN\n";
	my (@mgfs, @params, @zs);

	#################  Charges  #################
    my @zetas = ($self->{chbx_1}->GetValue(), $self->{chbx_2}->GetValue(),
                 $self->{chbx_3}->GetValue(), $self->{chbx_4}->GetValue(),
                 $self->{chbx_5}->GetValue());

    for (0..$#zetas){
        if ($zetas[$_]){
            push @zs, $_ + 1;
        }
	}
	unless (@zs){
		Wx::MessageBox('You must specify at least one charge',
					   'Missing arguments', Wx::wxOK | Wx::wxCENTRE);
		return;
	}
	print "charges used: ", @zs, "\n";

	################# Sequest_path, XCorr cutoff, Output name ##################
	my $path_sequest = $self->{tc_sp}->GetValue();
	my $xcorr_threshold = $self->{tc_xt}->GetValue();
	my $report_name = $self->{tc_of}->GetValue();

	unless (-B$path_sequest) {
		Wx::MessageBox('The sequest path does not correspond to an executable file',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	print "Sequest path: ", $path_sequest, "\n";
	unless ($xcorr_threshold =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/){
		Wx::MessageBox('The Xcorr threshold is nor correct',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	print "XCorr minimum threshold: ", $xcorr_threshold, "\n";
	if (-f$report_name) {
		Wx::MessageBox('The output file still exists. Please, change the name',
					   'File exists', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	unless (open SAL, '>', $report_name){
		Wx::MessageBox('The output file is nor correct',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	close SAL;
	print "Output file: ", $report_name, "\n";

	#################   Mgf dirs & Params  ##########################
	my @dirs = ($self->{tc_mgf_1}->GetValue(),
                $self->{tc_mgf_2}->GetValue(), $self->{tc_mgf_3}->GetValue());
	my @pars = ($self->{tc_par_1}->GetValue(),
                $self->{tc_par_2}->GetValue(), $self->{tc_par_3}->GetValue());

    for (0..2) {
        my ($par, $dir) = ($pars[$_], $dirs[$_]);
        if ((-T$par) and (-d$dir)){
            opendir(DIR, $dir);
            my @files = grep(/\.mgf$/, readdir(DIR));
            foreach(@files){
                $_ = $dir . '\\' . $_;
            }
            closedir(DIR);
            foreach(@files){
                push @mgfs, $_;
                push @params, $par;
            }
        }
	}
	unless ((@mgfs) or (@params)) {
		Wx::MessageBox('You must specify at least one correct mgf-dir and params-file pair',
					   'Missing arguments', Wx::wxOK | Wx::wxCENTRE);
		return;
	}
	for (0..$#mgfs){
		print $mgfs[$_], ' -> ', $params[$_], "\n";
	}

	#################   Mgf files inspection   #####################
    $self->SetStatusText('Inspecting mgf files');
    $self->Update();
    $self->Refresh();
	my $num_mgfs = 0;
	my $num_charges_used = 1 + $#zs;
	foreach my $mgf(@mgfs){
		open my $MGF, '<', $mgf;
		while(<$MGF>){
			if($_ =~ /^BEGIN IONS/){
				$num_mgfs++;
			}
		}
        close $MGF;
	}
	$num_mgfs = $num_mgfs * $num_charges_used;
	$self->SetStatusText('Search starts');
    $self->Update();
    $self->Refresh();
	##################################################################
	use threads;
	my $thr = threads->new(\&sequest::main, \@mgfs, \@params, \@zs, $path_sequest,
						    $xcorr_threshold, $report_name, $num_mgfs, $self, $event);

	$self->SetStatusText('Ready');
	$self->Update();
	$self->Refresh();
}



package sequest;

use warnings;
use strict;
use Data::Dumper;
my $number_executions = 0;

sub main{
	my @mgfs = @{$_[0]};
	my @params = @{$_[1]};
	my @zs = @{$_[2]};
	my $path_sequest = $_[3];
	my $xcorr_threshold = $_[4];
	my $report_name = $_[5];
	my $num_mgfs = $_[6];
	my $self = $_[7];
	my $event = $_[8];

	my %results_sequest;
	my $number_executions = 0;
	$self->SetStatusText('Searching');
	$self->Update();
	$self->Refresh();
	for (0..$#mgfs) {
		$results_sequest{$mgfs[$_]} = exec_sequest($mgfs[$_], $params[$_], \@zs,
												   $path_sequest, $xcorr_threshold,
												   $num_mgfs, $self, $event);
		my $status = $number_executions . '/' . $num_mgfs . ' spectra searched';
	}
	report(\%results_sequest, $report_name);
	$self->SetStatusText('Ready');
	$self->Update();
	$self->Refresh();
	return 1;
}


sub exec_sequest{
	my $mgf = $_[0];
	my $params = $_[1];
	my @zs = @{$_[2]};
	my $path_sequest = $_[3];
	my $xcorr_threshold = $_[4];
	my $total_spectra = $_[5];
	my $self = $_[6];
	my $event = $_[7];

	my %results;
	open my $INMGF, '<', $mgf;
	$/ = 'END IONS';
	my @mgfs = <$INMGF>;
    close $INMGF;

	for (my $i=0; $i<$#mgfs+1; $i++){
		my @lineas = split /\n/, $mgfs[$i];
        if ($#lineas < 5){next;}       #para eliminar accidental salto de linea al final de file
		my ($nombre, $scan, $ms, $rt, $pepmass);
		my $espectro = '';

		foreach (@lineas){
			if ($_ =~ /^TITLE\=/){
				$nombre = $_;
				$nombre =~ s/^TITLE\=//;
				($nombre, $scan, $ms, $rt) = split /\,/, $nombre;
				$scan =~ s/Scan\://;
				if($scan =~ /\-/){
					$scan =~ s/\-/\./;
				}
				else {
					$scan = $scan . '.' . $scan;
				}
				$nombre = $nombre . '.' . $scan;
				next;
			}
			elsif ($_ =~ /^PEPMASS\=/){
				$pepmass = $_;
				$pepmass =~ s/PEPMASS\=//;
				next;
			}
			elsif ($_ =~ /^\D/){
				next;
			}
			elsif ($_ =~ /^\d/){
				$espectro = $espectro . "\n" . $_;
			}
		}

		foreach my $z(@zs){
			my $nombre_z = $nombre . '.' . $z . '.dta';
			my $mh1 = (($pepmass * $z) - $z) + 1;
			$mh1 = sprintf ("%.4f", $mh1);

            open my $DTA, '>', $nombre_z;
			print $DTA $mh1 . ' ' . $z;
			print $DTA $espectro;
            close $DTA;

            my $exec_string = $path_sequest . ' -P' . $params . ' ' . $nombre_z;
			print "===========================================================================\n";
			print "File searched: ", $nombre_z, "\n";
			my $result_sequest = `$exec_string`;
			print $result_sequest, "\n";
			print "===========================================================================\n";
			print "\n\n";
			$number_executions++;
			$self->SetStatusText('Searching: ' . $number_executions . '/'
                                               . $total_spectra . ' spectra');
			$self->Update();
			$self->Refresh();
			$results{$nombre_z}{'out'} = $result_sequest;
			$ms =~ s/MS\://;
			$results{$nombre_z}{'ms'} = $ms;

			unlink $nombre_z;           #borro dta
			$nombre_z =~ s/dta$/out/;
			unlink $nombre_z;           #borro out
		}
	}
	%results = parse_out(\%results);
	%results = remove_charge_redundancies(\%results,\@zs);

	foreach my $res(keys %results){
		unless($results{$res}{'params'}{'xcorr'}){
			delete $results{$res};
			next;
		}
		if($results{$res}{'params'}{'xcorr'} < $xcorr_threshold){
			delete $results{$res};
		}
	}
	return \%results;
}


sub parse_out{
	my %results = %{$_[0]};
	foreach my $result(keys %results){
		my $out = $results{$result}{'out'};
		my $modificaciones;
		my @lineas = split /\n/, $out;
		my @parts = split /\./, $result;
		my $cadena_espectro = $parts[0] . '.' . $parts[1] . '.' . $parts[2];
		my $num_linea = -1;
		my $adquiere_peptidos = 0;
		$results{$result}{'z'} = (split /\./, $result)[3];
		foreach my $linea(@lineas){
			$num_linea++;
			if($num_linea > 1){                   #Listado de modificaciones
				if($lineas[$num_linea-2] =~ /^ ion series/){
					$modificaciones = $linea;
					}
			}
			if ($linea =~ /^ \(M\+H\)\+ mass \= (.+) \~/){     #Masa experimental
				$results{$result}{'experimental_mass'} = $1;
			}
			if ($linea =~ /^  1\./){
					$adquiere_peptidos++;
					#Divido la linea en caracteres
					my $line_results = parse_out_peptide_line($linea, $modificaciones);
					$results{$result}{'params'} = $line_results;
				next;
			}
			if ($adquiere_peptidos){                     #elijo deltacn correcto
				$adquiere_peptidos++;
				my $line_results = parse_out_peptide_line($linea, $modificaciones);

				#Protein must differ from protein at line 1 to assign a DeltaCn
				if($results{$result}{'params'}{'protein'} ne ${$line_results}{'protein'}){
					unless(${$line_results}{'delta'} == 0){
						$results{$result}{'params'}{'deltacn'} = ${$line_results}{'delta'};
						last;
					}
				}
			}
		}
	}
	foreach my $result(keys %results){
		#Results validation y deltamass and D calculations
		unless($results{$result}{'params'}{'deltacn'}){
			$results{$result}{'params'}{'deltacn'} = 0;
		}
		unless($results{$result}{'params'}{'xcorr'}){
			$results{$result}{'params'}{'xcorr'} = 0.00000000001;
		}
		unless($results{$result}{'params'}{'ranksp'}){
			$results{$result}{'params'}{'ranksp'} = 0.00000000001;
		}
		my $peptide_wo_mods = $results{$result}{'params'}{'peptide'};

		$peptide_wo_mods =~ s/(\*|\#|\@|\^|\~|\$|\[|\])//g;

		my $length_peptide = length($peptide_wo_mods);
		if ($length_peptide < 5){
			delete $results{$result};
			next;
		}
		$results{$result}{'params'}
                {'deltamass'} = $results{$result}{'experimental_mass'} -
                                $results{$result}{'params'}{'mh'};

		$results{$result}{'params'}
                {'d'} = sprintf("%.4f",
                                (8.4 * ((log($results{$result}{'params'}{'xcorr'})) /
                                        (log($length_peptide)))) +
                                (7.4 * ($results{$result}{'params'}{'deltacn'})) -
                                (0.2 * (log($results{$result}{'params'}{'ranksp'}))) -
                                (0.3 * (abs($results{$result}{'params'}{'deltamass'}))) -
                                 0.96
                                 );
    }
	return %results;
}


sub remove_charge_redundancies{
	my %results = %{$_[0]};
	my @zs = @{$_[1]};
	my %results_procesados;
	my %espectros;
	foreach my $result(keys %results){
			my $out = $results{$result}{'out'};
			my @lineas = split /\n/, $out;
			my @parts = split /\./, $result;
			my $cadena_espectro = $parts[0] . '.' . $parts[1] . '.' . $parts[2];
			$espectros{$cadena_espectro} = 1;
	}
    #Proceso cargas: elimino la hipotesis de carga de menor XCorr
	foreach my $espec(keys %espectros){
		my @keyss;
		foreach(@zs){
			push @keyss, $espec . ".$_.dta";
		}
		my $key_wins = $keyss[0];
		my $d_wins = $results{$keyss[0]}{'params'}{'d'};
		unless($results{$keyss[1]}{'params'}{'d'}){
			$results_procesados{$espec} = $results{$key_wins};
			next;
		}
		else {
			for (my $i=1; $i<$#keyss+1; $i++){
				if ($results{$keyss[$i]}{'params'}{'d'} > $d_wins){
					$d_wins = $results{$keyss[$i]}{'params'}{'d'};
					$key_wins = $keyss[$i];
				}
			}
		}
		$results_procesados{$espec} = $results{$key_wins};
	}
	return %results_procesados;
}


sub parse_out_peptide_line{
	my $modificaciones = $_[1];
	my @sparams = split ' ', $_[0];

    my $peptide = $sparams[$#sparams];
	$peptide = (split /\./, $peptide)[1];

    my $count = $sparams[11];
    if($count =~ /\+\d/){
        $count =~ s/\+//;
    }
    else {
        $count = 0;
    }

	my %results_line;
	$results_line{'xcorr'} = $sparams[7];
	$results_line{'mh'} = $sparams[5];
	$results_line{'sp'} = $sparams[8];
	$results_line{'ions'} = $sparams[9];
	$results_line{'protein'} = $sparams[10];
	$results_line{'ranksp'} = $sparams[3];
	$results_line{'peptide'} = $peptide;
	$results_line{'peptide_unimod'} = translate_peptide($peptide, $modificaciones);
	$results_line{'count'} = $count;
	$results_line{'delta'} = $sparams[6];

	return \%results_line;
}


sub report{
	my %results_procesados = %{$_[0]};
	my $report_name = $_[1];
    open my $OUT, '>', $report_name;
    print $OUT "File\t";
	print $OUT "scan 1\t";
	print $OUT "scan n\t";
	print $OUT "ms_level\t";
	print $OUT "reference\t";
	print $OUT "actual_mass\t";
	print $OUT "dmass\t";
	print $OUT "charge\t";
	print $OUT "peptide\t";
	print $OUT "Prob\t";
	print $OUT "xcorr\t";
	print $OUT "deltacn\t";
	print $OUT "sp\t";
	print $OUT "rsp\t";
	print $OUT "ions\t";
	print $OUT "count\t";
	print $OUT "D\n";

	foreach my $mgf(keys %results_procesados){
		foreach(keys %{$results_procesados{$mgf}}){
			my ($file, $scan1, $scan2) = split /\./, $_;
			print $OUT $file, "\t";
			print $OUT $scan1 . "\t" . $scan2, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'ms'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'protein'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'mh'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'deltamass'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'z'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'peptide_unimod'}, "\t";
			print $OUT "0\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'xcorr'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'deltacn'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'sp'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'ranksp'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'ions'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'count'}, "\t";
			print $OUT $results_procesados{$mgf}{$_}{'params'}{'d'}, "\t";
			print $OUT "\n";
		}
	}
	close $OUT;
}


sub translate_peptide{
	my ($pep, $mod) = @_;
	chomp $mod;
	my %detected_mods;
    my @signs = ('\*', '\^', '\#', '\@', '\~', '\$', '\]');

    foreach my $sign(@signs){
        my $patt = '\((\D+)' . $sign . ' (\+|\-)(\d+\.\d+)';
        if ($mod =~ /$patt/){
            my $delta = $2.$3;
            if((15.5 < $delta) and (16.5 > $delta)){
                $detected_mods{$sign} = '(35)';
            }
            if((-18.5 < $delta) and (-17.5 > $delta)){
                $detected_mods{$sign} = '(23)';
            }
            if((79.5 < $delta) and (81.5 > $delta)){
                $detected_mods{$sign} = '(21)';
            }
            if((143.6 < $delta) and (144.6 > $delta)){
                $detected_mods{$sign} = '(214)';
            }
            if((228.7 < $delta) and (229.7 > $delta)){
                $detected_mods{$sign} = '(737)';
            }
        }
    }
	foreach(keys %detected_mods){
		$pep =~ s/$_/$detected_mods{$_}/g;
	}
	return $pep;

}



package main;
use Wx::App;

undef &Wx::App::OnInit;
*Wx::App::OnInit = sub{1};
my $app = Wx::App->new();
Wx::InitAllImageHandlers();

my $frame = MySequestguiFrame->new();

$app->SetTopWindow($frame);
$frame->Show(1);
$app->MainLoop();

1;                         #608
