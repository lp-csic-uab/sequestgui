#!/usr/bin/perl -w
use strict;

sub clasic{
    my $dir1 = "dir1";
	my $dir2 = "dir2";
	my $dir3 = "dir3";
	my $par1 = "par1";
	my $par2 = "par2";
	my $par3 = "par3";

    print $dir1, $par1, "\n";
    print $dir2, $par2, "\n";
    print $dir3, $par3, "\n";
}

sub new{
    my @var = (("dir1", "par1"),
                ("dir2", "par2"),
                ("dir3", "par3"));

    while (@var) {
        my $dir = shift @var;
        my $par = shift @var;
        print $dir, $par, "\n";
    }
}
clasic();
new();
