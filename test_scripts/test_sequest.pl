package sequest;

#uses sequest outs to build excel output file
#allows checking sequestgui without requiring sequest

use warnings;
use strict;
use Data::Dumper;
my $number_executions = 0;

sub main{
	my $dir = "C:\\test_sequest";
	my @zs = (2,3,4);
	my $xcorr_threshold = 2;
	my $report_name = "aname3.xls";
	my @outs;
	my %bigresult;
	my %results;
	opendir(DIR, $dir);
	my @files = grep(/\.out/, readdir(DIR));
	closedir(DIR);
	foreach(@files){
		print $_, "\n";
		$_ = $dir . '\\' . $_;
		my $dta_name = $_;
		$dta_name =~ s/out/dta/;

		open my $OUT, '<', $_;
		my $result_sequest = "";
		while (<$OUT>) {
			$result_sequest = $result_sequest . $_;
		}

		print "line", $result_sequest;
		$results{$dta_name}{'out'} = $result_sequest;
		$results{$dta_name}{'ms'} = 2;  #probablemente no importa
		close $OUT;
	}

	%results = parse_out(\%results);
	%results = remove_charge_redundancy(\%results,\@zs);

	foreach my $res(keys %results){
		unless($results{$res}{'params'}{'xcorr'}){
			delete $results{$res};
			next;
		}
		if($results{$res}{'params'}{'xcorr'} < $xcorr_threshold){
			delete $results{$res};
		}
	}
    $bigresult{'mgf1'} = \%results;
	report(\%bigresult, $report_name);

	return 1;
}

sub parse_out{
	my %results = %{$_[0]};

	foreach my $dta_name(keys %results){
		#
		#$out (PME6_FT_Phospho_repl3.2520.2522.2.out) ->
		#
		#PME6_FT_Phospho_repl3.2520.2522.2.out
		#SEQUEST v.28 (rev. 12), (c) 1998-2007
		#Molecular Biotechnology, Univ. of Washington, J.Eng/S.Morgan/J.Yates
		#Licensed to Thermo Fisher Scientific Inc.
		#10/08/2010, 05:10 PM, 0.1 sec. on DEPT2
		#(M+H)+ mass = 1417.50000 ~ 0.0354 (+2), fragment tol = 1.0000, MONO/MONO
		#total inten = 7035.7, lowest Sp = 236.9, # matched peptides = 1321
		## amino acids = 23302, # proteins = 40590, C:\Xcalibur\database\PME6_decoy.fasta, C:\Xcalibur\database\PME6_decoy.fasta.hdr
		#ion series nABY ABCDVWXYZ: 0 1 1 0.0 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0
		#display top 10/5, ion % = 0.0, CODE = 011040
		#(M* +15.99492) (STY# +79.96633) C=160.03068  Enzyme:Trypsin(KR/P) (2)
		#
		# #   Rank/Sp      Id#     (M+H)+    deltCn   XCorr    Sp    Ions  Reference               Peptide
		#---  --------  --------  --------   ------  ------   -----  ----  ---------               -------
		# 1.   1 /  1       6453 1417.49377  0.0000  4.2869  1621.6  25/33  P02765|FETUA_HUMAN      K.CDSSPDS#AEDVR.K
		# 2.   2 /  2       6453 1417.49377  0.2253  3.3210   736.1  20/33  P02765|FETUA_HUMAN      -.CDSS#PDSAEDVR.-
		# 3.   3 /  3       6453 1417.49377  0.3377  2.8390   516.3  18/33  P02765|FETUA_HUMAN      -.CDS#SPDSAEDVR.-
		# 4.   4 / 12       5613 1417.52571  0.6369  1.5566   272.5  15/40  Q9BXX0|EMIL2_HUMAN      -.HAT#QDDAS#RTR.-
		# 5.   5 /  6      32900 1417.52016  0.6471  1.5129   340.8  17/45  rev_P41217|OX2G_HUMAN   -.LS#APT#Y#LQER.-
		# ...........................................................................................................
		#
		my $out = $results{$dta_name}{'out'};
		my $modifications;
		my @lines = split /\n/, $out;
		my @parts = split /\./, $dta_name;
		#
		# $cadena_espectro = PME6_FT_Phospho_repl3.2520.2522
		my $cadena_espectro = $parts[0] . '.' . $parts[1] . '.' . $parts[2];
		$results{$dta_name}{'z'} = $parts[3];
		my $line_num = -1;
		my $autoscan_hit_lines = 0;
		my $delta = 0;
		foreach my $line(@lines){
			$line_num++;
			if($line_num > 1){
				#Listado de modifications
				# ion series nABY ABCDVWXYZ: 0 1 1 0.0 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0
				# display top 10/5, ion % = 0.0, CODE = 011040
				# (M* +15.99492) (STY# +79.96633) C=160.03068  Enzyme:Trypsin(KR/P) (2)
				if($lines[$line_num-2] =~ /^ ion series/){
					$modifications = $line;
					}
			}
			#Masa experimental
			if ($line =~ /^ \(M\+H\)\+ mass \= (.+) \~/){
				$results{$dta_name}{'experimental_mass'} = $1;
			}
			if ($line =~ /^  1\./){
				$autoscan_hit_lines = 1;
				#Split the line into params
				my $line_results = parse_out_peptide_line($line, $modifications);
				$results{$dta_name}{'params'} = $line_results;
				next;
			}
			if ($autoscan_hit_lines){                     #elijo deltacn correcto
				#if the line is empty use previous or default delta value
				if (length($line) < 10){
					$results{$dta_name}{'params'}{'deltacn'} = $delta;
					last;
				}
				my $line_results = parse_out_peptide_line($line, $modifications);

				#Protein must differ from protein at line 1 to assign a DeltaCn
				if($results{$dta_name}{'params'}{'protein'} ne ${$line_results}{'protein'}){
					$delta = ${$line_results}{'delta'};
					unless($delta == 0){
						$results{$dta_name}{'params'}{'deltacn'} = $delta;
						last;
					}
				}
			}
		}
		# if there were no lines with hits in the sequest .out file...
		if ($autoscan_hit_lines == 0){
			delete $results{$dta_name};
		}
	}
	foreach my $dta_name(keys %results){
		#Results validation and deltamass and D calculations
		unless($results{$dta_name}{'params'}{'deltacn'}){
			$results{$dta_name}{'params'}{'deltacn'} = 0;
		}
		unless($results{$dta_name}{'params'}{'xcorr'}){
			$results{$dta_name}{'params'}{'xcorr'} = 0.00000000001;
		}
		unless($results{$dta_name}{'params'}{'ranksp'}){
			$results{$dta_name}{'params'}{'ranksp'} = 0.00000000001;
		}
		my $peptide_wo_mods = $results{$dta_name}{'params'}{'peptide'};

		$peptide_wo_mods =~ s/(\*|\#|\@|\^|\~|\$|\[|\])//g;

		my $length_peptide = length($peptide_wo_mods);
		if ($length_peptide < 5){
			delete $results{$dta_name};
			next;
		}
		$results{$dta_name}{'params'}
                {'deltamass'} = $results{$dta_name}{'experimental_mass'} -
                                $results{$dta_name}{'params'}{'mh'};

		$results{$dta_name}{'params'}
                {'d'} = sprintf("%.4f",
                                (8.4 * ((log($results{$dta_name}{'params'}{'xcorr'})) /
                                        (log($length_peptide)))) +
                                (7.4 * ($results{$dta_name}{'params'}{'deltacn'})) -
                                (0.2 * (log($results{$dta_name}{'params'}{'ranksp'}))) -
                                (0.3 * (abs($results{$dta_name}{'params'}{'deltamass'}))) -
                                 0.96
                                 );
    }
	return %results;
}


sub remove_charge_redundancy{
	my %results = %{$_[0]};
	my @zs = @{$_[1]};
	my %processed_results;
	my %spectra;
	foreach my $dta_name(keys %results){
		my $out = $results{$dta_name}{'out'};
		my @lineas = split /\n/, $out;
		my @parts = split /\./, $dta_name;
		#
		# $core_name = PME6_FT_Phospho_repl3.2520.2522
		my $core_name = $parts[0] . '.' . $parts[1] . '.' . $parts[2];

		$spectra{$core_name} = 1;
	}
    #keep only the charge assignation producing higher D-value
	foreach my $core_name(keys %spectra){
		my @valid_dtas;
		foreach(@zs){
			my $dta_name = $core_name . ".$_.dta";
			# collect only those file names still in %results
			#print $dta_name, "\n";
            if ($results{$dta_name}){
                #print 'push', "\n";
				push @valid_dtas, $dta_name;
			}
		}
		my $key_wins = $valid_dtas[0];
        #print 'name ', $core_name, "\n";
		my $d_wins = $results{$valid_dtas[0]}{'params'}{'d'};    ########
		# if there is only one charge, take this result
		unless($valid_dtas[1] and $results{$valid_dtas[1]}{'params'}{'d'}){
        #unless($results{$valid_dtas[1]}{'params'}{'d'}){
            #print 'one', "\n";
			$processed_results{$core_name} = $results{$key_wins};
			next;
		}
		else {
			for (my $i=1; $i<$#valid_dtas+1; $i++){
				if ($results{$valid_dtas[$i]}{'params'}{'d'} > $d_wins){
					$d_wins = $results{$valid_dtas[$i]}{'params'}{'d'};
					$key_wins = $valid_dtas[$i];
				}
			}
		}
		$processed_results{$core_name} = $results{$key_wins};
	}
	return %processed_results;
}


sub parse_out_peptide_line{
	my $modifications = $_[1];
	my @sparams = split ' ', $_[0];

    my $peptide = $sparams[$#sparams];
	$peptide = (split /\./, $peptide)[1];

    my $count = $sparams[11];
    if($count =~ /\+\d/){
        $count =~ s/\+//;
    }
    else {
        $count = 0;
    }

	my %results_line;
	$results_line{'xcorr'} = $sparams[7];
	$results_line{'mh'} = $sparams[5];
	$results_line{'sp'} = $sparams[8];
	$results_line{'ions'} = $sparams[9];
	$results_line{'protein'} = $sparams[10];
	$results_line{'ranksp'} = $sparams[3];
	$results_line{'peptide'} = $peptide;
	$results_line{'peptide_unimod'} = translate_peptide($peptide, $modifications);
	$results_line{'count'} = $count;
	$results_line{'delta'} = $sparams[6];

	return \%results_line;
}


sub report{
	my %processed_results = %{$_[0]};
	my $report_name = $_[1];
    open my $OUT, '>', $report_name;
    print $OUT "File\t";
	print $OUT "scan 1\t";
	print $OUT "scan n\t";
	print $OUT "ms_level\t";
	print $OUT "reference\t";
	print $OUT "actual_mass\t";
	print $OUT "dmass\t";
	print $OUT "charge\t";
	print $OUT "peptide\t";
	print $OUT "Prob\t";
	print $OUT "xcorr\t";
	print $OUT "deltacn\t";
	print $OUT "sp\t";
	print $OUT "rsp\t";
	print $OUT "ions\t";
	print $OUT "count\t";
	print $OUT "D\n";

	foreach my $mgf(keys %processed_results){
		foreach(keys %{$processed_results{$mgf}}){
			my ($file, $scan1, $scan2) = split /\./, $_;
			print $OUT $file, "\t";
			print $OUT $scan1 . "\t" . $scan2, "\t";
			print $OUT $processed_results{$mgf}{$_}{'ms'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'protein'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'mh'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'deltamass'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'z'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'peptide_unimod'}, "\t";
			print $OUT "0\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'xcorr'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'deltacn'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'sp'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'ranksp'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'ions'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'count'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'d'}, "\t";
			print $OUT "\n";
		}
	}
	close $OUT;
}


sub translate_peptide{
	my ($pep, $mod) = @_;
	chomp $mod;
	my %detected_mods;
    my @signs = ('\*', '\^', '\#', '\@', '\~', '\$', '\]');

    foreach my $sign(@signs){
        my $patt = '\((\D+)' . $sign . ' (\+|\-)(\d+\.\d+)';
        if ($mod =~ /$patt/){
            my $delta = $2.$3;
            if((15.5 < $delta) and (16.5 > $delta)){
                $detected_mods{$sign} = '(35)';
            }
            if((-18.5 < $delta) and (-17.5 > $delta)){
                $detected_mods{$sign} = '(23)';
            }
            if((79.5 < $delta) and (81.5 > $delta)){
                $detected_mods{$sign} = '(21)';
            }
            if((143.6 < $delta) and (144.6 > $delta)){
                $detected_mods{$sign} = '(214)';
            }
            if((228.7 < $delta) and (229.7 > $delta)){
                $detected_mods{$sign} = '(737)';
            }
        }
    }
	foreach(keys %detected_mods){
		$pep =~ s/$_/$detected_mods{$_}/g;
	}
	return $pep;
}


package main;
sequest::main;
1;                         #608
