
---

**WARNING!**: This is the *Old* source-code repository for SequestGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sequestgui/) located at https://sourceforge.net/p/lp-csic-uab/sequestgui/**  

---  
  
  
![https://lh4.googleusercontent.com/-akcJi11dE_c/TzFjUMSi6hI/AAAAAAAAARI/y54_RAWPR2M/s800/SequestGui%2520logo.png](https://lh4.googleusercontent.com/-akcJi11dE_c/TzFjUMSi6hI/AAAAAAAAARI/y54_RAWPR2M/s800/SequestGui%2520logo.png)


---

**WARNING!**: This is the *Old* source-code repository for SequestGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sequestgui/) located at https://sourceforge.net/p/lp-csic-uab/sequestgui/**  

---  
  
  
**Table Of Contents:**

[TOC]

#### Description
**`SequestGui`** is a [Perl](http://en.wikipedia.org/wiki/Perl) application that acts as a [front-end](http://en.wikipedia.org/wiki/Front_and_back_ends#Computer_science) (or [GUI](http://en.wikipedia.org/wiki/Graphical_user_interface)) to the [Sequest�](http://en.wikipedia.org/wiki/SEQUEST) search engine.

This way, `SequestGui` allows to easily use Sequest� [CLI](http://en.wikipedia.org/wiki/Command-line_interface) (sequest.exe) to the identification of MS/MS spectra from MGF (Mascot Generic Format) files.

The output of searches is saved as a [XLS](http://en.wikipedia.org/wiki/Microsoft_Excel_file_format#File_formats) spreadsheet file.

![https://lh3.googleusercontent.com/-IDcJ7KUkLNI/TzFjKYNcrZI/AAAAAAAAARg/RZN5k8yy81U/s800/SequestGui%2520screenshot%25201.png](https://lh3.googleusercontent.com/-IDcJ7KUkLNI/TzFjKYNcrZI/AAAAAAAAARg/RZN5k8yy81U/s800/SequestGui%2520screenshot%25201.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### External Requirements

  * Only the [Sequest�](http://www.ncbi.nlm.nih.gov/pubmed/24226387) search engine executable (found as part of `Thermo BioWorks` and [Thermo Proteome Discoverer](http://www.thermoscientific.com/en/product/proteome-discoverer-software.html)), if you use the [Windows Installer](https://googledrive.com/host/0B0ggtLTCQIP3ekU2QTRIX2stdFk/SequestGui_1.2.0_setup.exe) (see below).

##### From Windows Installer

  1. Download `SequestGui_x.y.z_setup.exe` [Windows Installer](https://bitbucket.org/lp-csic-uab/sequestgui/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `SequestGui` by double-clicking on the `SequestGui` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install [Perl](http://www.perl.org/) and other third party software indicated in [Dependencies](#markdown-header-source-dependencies), as required.
  1. Download `SequestGui` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/sequestgui/src).
  1. Copy the `SequestGui` folder in your path. If you only plan to run the program from a particular user, you will need only to put `SequestGui` folder inside a user folder.
  1. Run `SequestGui` by double-clicking on the `sequestgui.pl` module, or typing  `perl sequestgui.pl`  on the command line.

###### _Source Dependencies:_

  * [Perl](http://www.perl.org/) 5.16 (not tested with other versions)
  * [wxPerl](http://www.wxperl.it/) 0.9921 (with Alien::wxWidgets 0.64)

Third-party software and package versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

#### Download

You can download the Windows Installer for the last version of **`SequestGui`** [here](https://bitbucket.org/lp-csic-uab/sequestgui/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/sequestgui/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code.


---

**WARNING!**: This is the *Old* source-code repository for SequestGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/sequestgui/) located at https://sourceforge.net/p/lp-csic-uab/sequestgui/**  

---  
  
  
