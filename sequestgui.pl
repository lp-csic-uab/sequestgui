#!/usr/bin/perl -w --

use utf8;
use Wx 0.15 qw[:allclasses];
use strict;
use warnings;
use Data::Dumper;

package MySequestguiFrame;

use Wx qw[:everything];
use Wx::Event qw(EVT_BUTTON);
use warnings;
use strict;

use SequestguiFrame;
use base qw(SequestguiFrame);

my $title = "SequestGui 0.4";
my $sequest_path = "C:\\Xcalibur\\system\\programs\\BioworksBrowser\\sequest.exe";
my $xc_threshold = 0.2;

sub new {
	my $class = shift;
	my $self = $class->SUPER::new();

	$self->SetTitle($title);
    $self->{tc_sp}->SetValue($sequest_path);
    $self->{tc_xt}->SetValue($xc_threshold);
    $self->{chbx_2}->SetValue(1);
    $self->{chbx_3}->SetValue(1);

    EVT_BUTTON( $self, $self->{bt_mgf_1}, \&set_mgf_1);
	EVT_BUTTON( $self, $self->{bt_mgf_2}, \&set_mgf_2);
	EVT_BUTTON( $self, $self->{bt_mgf_3}, \&set_mgf_3);

    EVT_BUTTON( $self, $self->{bt_par_1}, \&set_par_1);
	EVT_BUTTON( $self, $self->{bt_par_2}, \&set_par_2);
    EVT_BUTTON( $self, $self->{bt_par_3}, \&set_par_3);

	EVT_BUTTON( $self, $self->{bt_sp}, \&set_exec);
	EVT_BUTTON( $self, $self->{bt_of}, \&set_outfile);
    EVT_BUTTON( $self, $self->{bt_run}, \&runit);

	bless($self, $class); # reconsecrate
    return $self;
}


sub set_mgf_1 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_1');
}

sub set_mgf_2 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_2');
}

sub set_mgf_3 {
	my ($self, $event) = @_;
    $self->get_dir('tc_mgf_3');
}

sub set_par_1{
	my ($self, $event) = @_;
    $self->get_files('tc_par_1');
}

sub set_par_2{
	my ($self, $event) = @_;
    $self->get_files('tc_par_2');
}

sub set_par_3{
	my ($self, $event) = @_;
    $self->get_files('tc_par_3');
}

sub get_dir {
	my ($self, $win) = @_;
	my $dialog = Wx::DirDialog->new(undef, 'Select the directory with the mgf files', '');
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $dir = $dialog->GetPath;
	$self->{$win}->SetValue($dir);
}

sub get_files{
    my ($self, $win) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select a .params file', '',
                                     ".params", "*.params", Wx::wxFD_OPEN);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{$win}->SetValue($default_dir . '\\' . $filename);
}


sub set_exec{
	my ($self, $event) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select the Sequest executable', '',
                                     ".exe", "*.exe", Wx::wxFD_OPEN);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{tc_sp}->SetValue($default_dir . '\\' . $filename);
}

sub set_outfile{
	my ($self, $event) = @_;
	my $dialog = Wx::FileDialog->new(undef, 'Select the Results file', '',
                                     ".xls", "*.xls", Wx::wxFD_SAVE);
	if ($dialog->ShowModal == Wx::wxID_CANCEL) {
		return;
	}
	my $filename = $dialog->GetFilename;
	my $default_dir = $dialog->GetDirectory;
	$self->{tc_of}->SetValue($default_dir . '\\' . $filename);
}

sub runit {
	my ($self, $event) = @_;
	print "RUN\n";
	my (@mgfs, @params, @zs);

	#################  Charges  #################
    my @zetas = ($self->{chbx_1}->GetValue(), $self->{chbx_2}->GetValue(),
                 $self->{chbx_3}->GetValue(), $self->{chbx_4}->GetValue(),
                 $self->{chbx_5}->GetValue());

    for (0..$#zetas){
        if ($zetas[$_]){
            push @zs, $_ + 1;
        }
	}
	unless (@zs){
		Wx::MessageBox('You must specify at least one charge',
					   'Missing arguments', Wx::wxOK | Wx::wxCENTRE);
		return;
	}
	print "charges used: ", @zs, "\n";

	################# Sequest_path, XCorr cutoff, Output name ##################
	my $path_sequest = $self->{tc_sp}->GetValue();
	my $xcorr_threshold = $self->{tc_xt}->GetValue();
	my $report_name = $self->{tc_of}->GetValue();

	unless (-B$path_sequest) {
		Wx::MessageBox('The sequest path does not correspond to an executable file',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	print "Sequest path: ", $path_sequest, "\n";
	unless ($xcorr_threshold =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/){
		Wx::MessageBox('The Xcorr threshold is not correct',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	print "XCorr minimum threshold: ", $xcorr_threshold, "\n";
	if (-f$report_name) {
		Wx::MessageBox('The output file still exists. Please, change the name',
					   'File exists', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	unless (open SAL, '>', $report_name){
		Wx::MessageBox('The output file is not correct',
					   'Wrong arguments', Wx::wxOK | Wx::wxCENTRE );
		return;
	}
	close SAL;
	print "Output file: ", $report_name, "\n";

	#################   Mgf dirs & Params  ##########################
	my @dirs = ($self->{tc_mgf_1}->GetValue(),
                $self->{tc_mgf_2}->GetValue(), $self->{tc_mgf_3}->GetValue());
	my @pars = ($self->{tc_par_1}->GetValue(),
                $self->{tc_par_2}->GetValue(), $self->{tc_par_3}->GetValue());

    for (0..2) {
        my ($par, $dir) = ($pars[$_], $dirs[$_]);
        if ((-T$par) and (-d$dir)){
            opendir(DIR, $dir);
            my @files = grep {/\.mgf$/} readdir(DIR);
            foreach(@files){
                $_ = $dir . '\\' . $_;
            }
            closedir(DIR);
            foreach(@files){
                push @mgfs, $_;
                push @params, $par;
            }
        }
	}
	unless ((@mgfs) or (@params)) {
		Wx::MessageBox('You must specify at least one correct mgf-dir and params-file pair',
					   'Missing arguments', Wx::wxOK | Wx::wxCENTRE);
		return;
	}
	for (0..$#mgfs){
		print $mgfs[$_], ' -> ', $params[$_], "\n";
	}

	#################   Mgf files inspection   #####################
    $self->SetStatusText('Inspecting mgf files');
    $self->Update();
    $self->Refresh();
	my $num_spectra = 0;
	my $num_charges_used = 1 + $#zs;
	foreach my $mgf(@mgfs){
		open my $MGF, '<', $mgf;
		while(<$MGF>){
			if($_ =~ /^BEGIN IONS/){
				$num_spectra++;
			}
		}
        close $MGF;
	}
	my $num_dtas = $num_spectra * $num_charges_used;
	$self->SetStatusText('Search starts');
    $self->Update();
    $self->Refresh();
	##################################################################
	use threads;
	my $thr = threads->new(\&sequest::main, \@mgfs, \@params, \@zs, $path_sequest,
						    $xcorr_threshold, $report_name, $num_dtas, $self, $event);

	$self->SetStatusText('Ready');
	$self->Update();
	$self->Refresh();
}



package sequest;

use warnings;
use strict;
use Data::Dumper;
my $number_executions = 0;

sub main{
	my @mgfs = @{$_[0]};
	my @params = @{$_[1]};
	my @zs = @{$_[2]};
	my $path_sequest = $_[3];
	my $xcorr_threshold = $_[4];
	my $report_name = $_[5];
	my $num_dtas = $_[6];
	my $self = $_[7];
	my $event = $_[8];

	my %results_sequest;
	$self->SetStatusText('Searching');
	$self->Update();
	$self->Refresh();
	for (0..$#mgfs) {
		$results_sequest{$mgfs[$_]} = exec_sequest($mgfs[$_], $params[$_], \@zs,
												   $path_sequest, $xcorr_threshold,
												   $num_dtas, $self, $event);
	}
	report(\%results_sequest, $report_name);
	$self->SetStatusText('Ready');
	$self->Update();
	$self->Refresh();
	return 1;
}


sub exec_sequest{
	my $mgf = $_[0];
	my $params = $_[1];
	my @zs = @{$_[2]};
	my $path_sequest = $_[3];
	my $xcorr_threshold = $_[4];
	my $num_dtas = $_[5];
	my $self = $_[6];
	my $event = $_[7];

	my %results;
	open my $INMGF, '<', $mgf;
	$/ = 'END IONS';
	my @mgfs = <$INMGF>;
    close $INMGF;
    #
	#Mascot file format:
	#
	#BEGIN IONS
	#TITLE=PME6_FT_Phospho_repl3,Scan:2520-2522,MS:2,Rt:3456.282
	#PEPMASS=709.25
	#SCANS=2520-2522
	#RTINSECONDS=3456.282
	#213.021 7.66
	#230.038 13.42
	#............
	#END IONS
	#
	for (my $i=0; $i<$#mgfs+1; $i++){
		my @lines = split /\n/, $mgfs[$i];
		#to eliminate accidental line break at the end of file:
        if ($#lines < 5){next;}
		my ($name, $scan, $ms, $rt, $pepmass);
		my $espectro = '';

		foreach (@lines){
			#TITLE=PME6_FT_Phospho_repl3,Scan:2520-2522,MS:2,Rt:3456.282
			if ($_ =~ /^TITLE\=/){
				$name = $_;
				$name =~ s/^TITLE\=//;
				($name, $scan, $ms, $rt) = split /\,/, $name;
				$scan =~ s/Scan\://;
				if($scan =~ /\-/){
					$scan =~ s/\-/\./;                  #2520-2522 -> 2520.2522
				}
				else {
					$scan = $scan . '.' . $scan;        #1432      -> 1432.1432
				}
				$name = $name . '.' . $scan;
				next;
			}
			#PEPMASS=709.25
			elsif ($_ =~ /^PEPMASS\=/){
				$pepmass = $_;
				$pepmass =~ s/PEPMASS\=//;
				next;
			}
			#if no number, pass
			elsif ($_ =~ /^\D/){
				next;
			}
			#if number, append the line
			elsif ($_ =~ /^\d/){
				$espectro = $espectro . "\n" . $_;
			}
		}

		foreach my $z(@zs){
			# $name = PME6_FT_Phospho_repl3.2520.2522
			# $dta_name    = PME6_FT_Phospho_repl3.2520.2522.x.dta
			my $dta_name = $name . '.' . $z . '.dta';
			my $mh1 = (($pepmass * $z) - $z) + 1;
			$mh1 = sprintf ("%.4f", $mh1);

			# dta format
			# (nombre_z = PME6_FT_Phospho_repl3.2520.2522.2.dta)
			#
			#1417.5000 2
			#213.021 7.66
			#230.038 13.42
			#231.140 2.21
            #...........

			open my $DTA, '>', $dta_name;
			print $DTA $mh1 . ' ' . $z;
			print $DTA $espectro;
            close $DTA;

            my $exec_string = $path_sequest . ' -P' . $params . ' ' . $dta_name;
			print "===========================================================================\n";
			print "File searched: ", $dta_name, "\n";
			my $result_sequest = `$exec_string`;
			print $result_sequest, "\n";
			print "===========================================================================\n";
			print "\n\n";
			$number_executions++;
			$self->SetStatusText('Searching: ' . $number_executions . '/'
                                               . $num_dtas . ' spectra');
			$self->Update();
			$self->Refresh();

			$results{$dta_name}{'out'} = $result_sequest;
			# $ms = 'MS:2'
			$ms =~ s/MS\://;
			$results{$dta_name}{'ms'} = $ms;

			#unlink $dta_name;           #remove dta files
			$dta_name =~ s/dta$/out/;
			#unlink $dta_name;           #remove out files
		}
	}
	%results = parse_out(\%results);
	%results = remove_charge_redundancy(\%results,\@zs);

	foreach my $res(keys %results){
		unless($results{$res}{'params'}{'xcorr'}){
			delete $results{$res};
			next;
		}
		if($results{$res}{'params'}{'xcorr'} < $xcorr_threshold){
			delete $results{$res};
		}
	}
	return \%results;
}


sub parse_out{
	my %results = %{$_[0]};

	foreach my $dta_name(keys %results){
		#
		#$out (PME6_FT_Phospho_repl3.2520.2522.2.out) ->
		#
		#PME6_FT_Phospho_repl3.2520.2522.2.out
		#SEQUEST v.28 (rev. 12), (c) 1998-2007
		#Molecular Biotechnology, Univ. of Washington, J.Eng/S.Morgan/J.Yates
		#Licensed to Thermo Fisher Scientific Inc.
		#10/08/2010, 05:10 PM, 0.1 sec. on DEPT2
		#(M+H)+ mass = 1417.50000 ~ 0.0354 (+2), fragment tol = 1.0000, MONO/MONO
		#total inten = 7035.7, lowest Sp = 236.9, # matched peptides = 1321
		## amino acids = 23302, # proteins = 40590, C:\Xcalibur\database\PME6_decoy.fasta, C:\Xcalibur\database\PME6_decoy.fasta.hdr
		#ion series nABY ABCDVWXYZ: 0 1 1 0.0 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0
		#display top 10/5, ion % = 0.0, CODE = 011040
		#(M* +15.99492) (STY# +79.96633) C=160.03068  Enzyme:Trypsin(KR/P) (2)
		#
		# #   Rank/Sp      Id#     (M+H)+    deltCn   XCorr    Sp    Ions  Reference               Peptide
		#---  --------  --------  --------   ------  ------   -----  ----  ---------               -------
		# 1.   1 /  1       6453 1417.49377  0.0000  4.2869  1621.6  25/33  P02765|FETUA_HUMAN      K.CDSSPDS#AEDVR.K
		# 2.   2 /  2       6453 1417.49377  0.2253  3.3210   736.1  20/33  P02765|FETUA_HUMAN      -.CDSS#PDSAEDVR.-
		# 3.   3 /  3       6453 1417.49377  0.3377  2.8390   516.3  18/33  P02765|FETUA_HUMAN      -.CDS#SPDSAEDVR.-
		# 4.   4 / 12       5613 1417.52571  0.6369  1.5566   272.5  15/40  Q9BXX0|EMIL2_HUMAN      -.HAT#QDDAS#RTR.-
		# 5.   5 /  6      32900 1417.52016  0.6471  1.5129   340.8  17/45  rev_P41217|OX2G_HUMAN   -.LS#APT#Y#LQER.-
		# ...........................................................................................................
		#
		my $out = $results{$dta_name}{'out'};
		my $modifications;
		my @lines = split /\n/, $out;
		my @parts = split /\./, $dta_name;
		#
		# $cadena_espectro = PME6_FT_Phospho_repl3.2520.2522
		my $cadena_espectro = $parts[0] . '.' . $parts[1] . '.' . $parts[2];
		$results{$dta_name}{'z'} = $parts[3];
		my $line_num = -1;
		my $autoscan_hit_lines = 0;
		my $delta = 0;
		foreach my $line(@lines){
			$line_num++;
			if($line_num > 1){
				#Listado de modifications
				# ion series nABY ABCDVWXYZ: 0 1 1 0.0 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0
				# display top 10/5, ion % = 0.0, CODE = 011040
				# (M* +15.99492) (STY# +79.96633) C=160.03068  Enzyme:Trypsin(KR/P) (2)
				if($lines[$line_num-2] =~ /^ ion series/){
					$modifications = $line;
					}
			}
			#Masa experimental
			if ($line =~ /^ \(M\+H\)\+ mass \= (.+) \~/){
				$results{$dta_name}{'experimental_mass'} = $1;
			}
			if ($line =~ /^  1\./){
				$autoscan_hit_lines = 1;
				#Split the line into params
				my $line_results = parse_out_peptide_line($line, $modifications);
				$results{$dta_name}{'params'} = $line_results;
				next;
			}
			if ($autoscan_hit_lines){                     #elijo deltacn correcto
				#if the line is empty use previous or default delta value
				if (length($line) < 10){
					$results{$dta_name}{'params'}{'deltacn'} = $delta;
					last;
				}
				my $line_results = parse_out_peptide_line($line, $modifications);

				#Protein must differ from protein at line 1 to assign a DeltaCn
				if($results{$dta_name}{'params'}{'protein'} ne ${$line_results}{'protein'}){
					$delta = ${$line_results}{'delta'};
					unless($delta == 0){
						$results{$dta_name}{'params'}{'deltacn'} = $delta;
						last;
					}
				}
			}
		}
		# if there were no lines with hits in the sequest .out file...
		if ($autoscan_hit_lines == 0){
			delete $results{$dta_name};
		}
	}
	foreach my $dta_name(keys %results){
		#Results validation and deltamass and D calculations
		unless($results{$dta_name}{'params'}{'deltacn'}){
			$results{$dta_name}{'params'}{'deltacn'} = 0;
		}
		unless($results{$dta_name}{'params'}{'xcorr'}){
			$results{$dta_name}{'params'}{'xcorr'} = 0.00000000001;
		}
		unless($results{$dta_name}{'params'}{'ranksp'}){
			$results{$dta_name}{'params'}{'ranksp'} = 0.00000000001;
		}
		my $peptide_wo_mods = $results{$dta_name}{'params'}{'peptide'};

		$peptide_wo_mods =~ s/(\*|\#|\@|\^|\~|\$|\[|\])//g;

		my $length_peptide = length($peptide_wo_mods);
		if ($length_peptide < 5){
			delete $results{$dta_name};
			next;
		}
		$results{$dta_name}{'params'}
                {'deltamass'} = $results{$dta_name}{'experimental_mass'} -
                                $results{$dta_name}{'params'}{'mh'};

		$results{$dta_name}{'params'}
                {'d'} = sprintf("%.4f",
                                (8.4 * ((log($results{$dta_name}{'params'}{'xcorr'})) /
                                        (log($length_peptide)))) +
                                (7.4 * ($results{$dta_name}{'params'}{'deltacn'})) -
                                (0.2 * (log($results{$dta_name}{'params'}{'ranksp'}))) -
                                (0.3 * (abs($results{$dta_name}{'params'}{'deltamass'}))) -
                                 0.96
                                 );
    }
	return %results;
}


sub remove_charge_redundancy{
	my %results = %{$_[0]};
	my @zs = @{$_[1]};
	my %processed_results;
	my %spectra;
	foreach my $dta_name(keys %results){
		my $out = $results{$dta_name}{'out'};
		my @lineas = split /\n/, $out;
		my @parts = split /\./, $dta_name;
		#
		# $core_name = PME6_FT_Phospho_repl3.2520.2522
		my $core_name = $parts[0] . '.' . $parts[1] . '.' . $parts[2];

		$spectra{$core_name} = 1;
	}
    #keep only the charge assignation producing higher D-value
	foreach my $core_name(keys %spectra){
		my @valid_dtas;
		foreach(@zs){
			my $dta_name = $core_name . ".$_.dta";
			# collect only those file names still in %results
			if ($results{$dta_name}){
				push @valid_dtas, $dta_name;
			}
		}
		my $key_wins = $valid_dtas[0];
		my $d_wins = $results{$valid_dtas[0]}{'params'}{'d'};
		# if there is only one charge, take this result
		unless($valid_dtas[1] and $results{$valid_dtas[1]}{'params'}{'d'}){
			$processed_results{$core_name} = $results{$key_wins};
			next;
		}
		else {
			for (my $i=1; $i<$#valid_dtas+1; $i++){
				if ($results{$valid_dtas[$i]}{'params'}{'d'} > $d_wins){
					$d_wins = $results{$valid_dtas[$i]}{'params'}{'d'};
					$key_wins = $valid_dtas[$i];
				}
			}
		}
		$processed_results{$core_name} = $results{$key_wins};
	}
	return %processed_results;
}


sub parse_out_peptide_line{
	my $modifications = $_[1];
	my @sparams = split ' ', $_[0];

    my $peptide = $sparams[$#sparams];
	$peptide = (split /\./, $peptide)[1];

    my $count = $sparams[11];
    if($count =~ /\+\d/){
        $count =~ s/\+//;
    }
    else {
        $count = 0;
    }

	my %results_line;
	$results_line{'xcorr'} = $sparams[7];
	$results_line{'mh'} = $sparams[5];
	$results_line{'sp'} = $sparams[8];
	$results_line{'ions'} = $sparams[9];
	$results_line{'protein'} = $sparams[10];
	$results_line{'ranksp'} = $sparams[3];
	$results_line{'peptide'} = $peptide;
	$results_line{'peptide_unimod'} = translate_peptide($peptide, $modifications);
	$results_line{'count'} = $count;
	$results_line{'delta'} = $sparams[6];

	return \%results_line;
}


sub report{
	my %processed_results = %{$_[0]};
	my $report_name = $_[1];
    open my $OUT, '>', $report_name;
    print $OUT "File\t";
	print $OUT "scan 1\t";
	print $OUT "scan n\t";
	print $OUT "ms_level\t";
	print $OUT "reference\t";
	print $OUT "actual_mass\t";
	print $OUT "dmass\t";
	print $OUT "charge\t";
	print $OUT "peptide\t";
	print $OUT "Prob\t";
	print $OUT "xcorr\t";
	print $OUT "deltacn\t";
	print $OUT "sp\t";
	print $OUT "rsp\t";
	print $OUT "ions\t";
	print $OUT "count\t";
	print $OUT "D\n";

	foreach my $mgf(keys %processed_results){
		foreach(keys %{$processed_results{$mgf}}){
			my ($file, $scan1, $scan2) = split /\./, $_;
			print $OUT $file, "\t";
			print $OUT $scan1 . "\t" . $scan2, "\t";
			print $OUT $processed_results{$mgf}{$_}{'ms'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'protein'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'mh'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'deltamass'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'z'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'peptide_unimod'}, "\t";
			print $OUT "0\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'xcorr'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'deltacn'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'sp'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'ranksp'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'ions'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'count'}, "\t";
			print $OUT $processed_results{$mgf}{$_}{'params'}{'d'}, "\t";
			print $OUT "\n";
		}
	}
	close $OUT;
}


sub translate_peptide{
	my ($pep, $mod) = @_;
	chomp $mod;
	my %detected_mods;
    my @signs = ('\*', '\^', '\#', '\@', '\~', '\$', '\]');

    foreach my $sign(@signs){
        my $patt = '\((\D+)' . $sign . ' (\+|\-)(\d+\.\d+)';
        if ($mod =~ /$patt/){
            my $delta = $2.$3;
            if((15.5 < $delta) and (16.5 > $delta)){
                $detected_mods{$sign} = '(35)';
            }
            if((-18.5 < $delta) and (-17.5 > $delta)){
                $detected_mods{$sign} = '(23)';
            }
            if((79.5 < $delta) and (81.5 > $delta)){
                $detected_mods{$sign} = '(21)';
            }
            if((143.6 < $delta) and (144.6 > $delta)){
                $detected_mods{$sign} = '(214)';
            }
            if((228.7 < $delta) and (229.7 > $delta)){
                $detected_mods{$sign} = '(737)';
            }
        }
    }
	foreach(keys %detected_mods){
		$pep =~ s/$_/$detected_mods{$_}/g;
	}
	return $pep;

}



package main;
use Wx::App;

undef &Wx::App::OnInit;
*Wx::App::OnInit = sub{1};
my $app = Wx::App->new();
Wx::InitAllImageHandlers();

my $frame = MySequestguiFrame->new();

$app->SetTopWindow($frame);
$frame->Show(1);
$app->MainLoop();

1;                         #608
